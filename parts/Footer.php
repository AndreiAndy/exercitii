<tr style="background-color:#e7eff8;">
    <td>
        <center>
            <table width="1200" >
                <tr>
                    <td style="color: blue">Servicii pentru clienti
                        <ul style="color: grey" style="list-style-type: none">
                            <li>Deschiderea coletului la livrare</li>
                            <li>30 de zile drept de retur</li>
                            <li>Garantii si service</li>
                            <li>Black friday eMAG</li>
                            <li>Plata cu cardul in rate fara dobanda</li>
                            <li>Finantare in rate prin eCREDIT</li>
                        </ul>
                    </td>
                    <td style="color: blue">Comenzi si livrare
                        <ul style="color: grey" style="list-style-type: none">
                            <li>Contul meu la eMAG</li>
                            <li>Cum comand online</li>
                            <li>Livrarea comenzilor</li>
                            <li>eMAG Corporate</li>
                            <li>eMAG Marketplace</li>
                            <li>Modalitati de finantare si plata</li>
                        </ul>
                    </td>
                    <td style="color: blue">Suport clienti
                        <ul style="color: grey" style="list-style-type: none">
                            <li>Formular reparatie produs</li>
                            <li>Formular retunare produs</li>
                            <li>Contact</li>
                            <li>Intrebari frecvente</li>
                            <li>Conditii generale privind furnizarea serviciilor postale</li>
                            <li>ANPC</li>
                        </ul>
                    </td>
                    <td style="color: blue">eMag.ro
                        <ul style="color: grey" style="list-style-type: none">
                            <li>Vreau sa vand pe eMAG</li>
                            <li>Termeni si conditii</li>
                            <li>Prelucrarea datelor cu caracter personal</li>
                            <li>Politica de utilizare Cookie-uri</li>
                            <li>ODR</li>
                            <li>Programele Fundatiei eMAG</li>
                        </ul>
                    </td>
                </tr>
            </table>
        </center>
    </td>
</tr>


</table>
</body>
</html>
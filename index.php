<?php
session_start();
if (!isset($_SESSION['column'])){
    $_SESSION['column']=0;
    $_SESSION['direction']='asc';
    $_SESSION['search']='';
}
if (isset($_POST['search'])){
    $_SESSION['search']= $_POST['search'];
}

function sortAsc($x, $y) {
    $column = $_SESSION['column'];
    return strcmp($x[$column],$y[$column]);
}

function sortDesc($x, $y) {
    $column = $_SESSION['column'];
    return strcmp($y[$column],$x[$column]);
}

function filter($line)
{
    if ($_SESSION['search']) {
        return strpos($line[3], $_SESSION['search']) || strpos($line[4], $_SESSION['search']);
    } else {
        return true;
    }
}



$data = array_map('str_getcsv', file('oscar_age_female1.csv'));
$header = array_shift($data);
?>
<form action="index.php" method="post">
    <input type="text" value="<?php echo $_SESSION['search']; ?>" name="search" placeholder="Search" />
    <input type="submit" value="Cauta">
</form>
<table border='1'>
<tr>
<?php
foreach ($header as $key => $value){
    echo "<th>$value&nbsp;&nbsp; 
<a href='index.php?direction=asc&column=$key'>&dArr; </a>&nbsp;&nbsp;
<a href='index.php?direction=desc&column=$key'>&uArr; </a>

</th>";
}
echo "</tr>";
if (isset($_GET['direction'])) {
    $_SESSION['column']=$_GET['column'];
    $_SESSION['direction']=$_GET['direction'];
}

if ($_SESSION['direction'] == 'asc') {
    usort($data, 'sortAsc');
} else {
    usort($data, 'sortDesc');
}


$maxPerPage = 20;
$currentPage = 1;
if (isset($_GET['page'])){
    $currentPage = $_GET['page'];
}

$data = array_filter($data, 'filter');

$nrOfPages = ceil(count($data)/$maxPerPage);

$startIndex = ($currentPage-1)*$maxPerPage;

$data = array_slice($data, $startIndex, $maxPerPage);

foreach ($data as $line){
    echo "<tr>";
    foreach ($line as $value){
        echo "<td>$value</td>";
    }
    echo "</tr>";
}

echo "</table>";




?>

<p style="text-align:center; font-size: 40px; font-weight: 600;">
    <?php if ($currentPage>1):?>
        <a href="index.php">&laquo;</a>
        <a href="index.php?page=<?php echo $currentPage-1; ?>">&lt;</a>
    <?php endif; ?>
    <?php echo $currentPage; ?>
    <?php if ($currentPage<$nrOfPages):?>
        <a href="index.php?page=<?php echo $currentPage+1; ?>">&gt;</a>
        <a href="index.php?page=<?php echo $nrOfPages; ?>">&raquo;</a>
    <?php endif; ?>
</p>

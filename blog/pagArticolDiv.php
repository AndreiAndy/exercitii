<?php
include "functions query.php";
$mysql = mysqli_connect('77.81.105.198', 'root', 'scoalait123', 'web-04-andrei');
$article=query('SELECT * FROM articole WHERE id='.$_GET['id']);
?>
<head>
    <meta charset="UTF-8">
    <title>Blog Andrei</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
<div id="wrapper">
    <div id="header" class="header"><h1>Blogul lui Andrei</h1></div>
    <div id="menu" class="menu">
        <?php $categories = query('SELECT * FROM categorie');
        foreach ($categories as $category) {
            echo '<a href="pagCategDiv.php?id=' . $category['Id'] . '" class="a">' . $category['Nume'] . '</a>';
        } ?>

    </div>
    <div id="maincontent" style="height: 600px">
        <div id="content" style="width: 70%; float:left">
            <h1>
                <?php echo  $article[0]['titlu'];?>
            </h1>
            &nbsp;&nbsp;
            <p>
                <?php echo  $article[0]['text'];?>
            </p>
            <p>
                <img src="photos/'.<?php $article[0]['poze'];?>.'">
            </p>
        </div>
        <div id="sidebar" style=" width:30%;float: right "><h2>Articole recente</h2>
            <ol>
                <?php
                $articles = query('SELECT * FROM articole');

                foreach ($articles as $article ){
                    echo '<li class="list-group-item"><a href="pagArticolDiv.php?id='.$article['id'].'">'.$article['titlu'].'</a></li>';

                }
                ?>
            </ol>
        </div>
        <div style="clear: both"></div>
    </div>
    <div id="footer" class="footer">&copy; 2019</div>
</div>
</body>
</html>
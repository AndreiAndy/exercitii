<?php
include "functions query.php";
$mysql = mysqli_connect('77.81.105.198', 'root', 'scoalait123', 'web-04-andrei');
?>
<head>
    <meta charset="UTF-8">
    <title>Blog Andrei</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
<div id="wrapper">
    <div id="header" class="header"><h1>Blogul lui Andrei</h1></div>
    <div id="menu" class="menu">
        <?php $categories = query('SELECT * FROM categorie');
        foreach ($categories as $category) {
            echo '<a href="pagCategDiv.php?id=' . $category['Id'] . '" class="a">' . $category['Nume'] . '</a>';
        } ?>

    </div>
    <div id="maincontent" style="height: 600px">
        <div id="content" style="width: 70%; float:left">
            <?php
             $maxPerPage = 3;
             $currentPage = 1;
        if (isset($_GET['page'])) {
            $currentPage = $_GET['page'];
        }

            $articlesOnCurrentPage = query('SELECT * FROM articole
            LIMIT 0,'.$maxPerPage.' ');
            ?>
                <?php
                foreach ($articlesOnCurrentPage as $articleOnCurrentPage){
                    echo

                            '<div  style="width: 30%; float:left">
                                <img src="photos/'.$articleOnCurrentPage['poze'].'" width="130" height="130">
                            </div>
                            <div style="width: 70%;float: right ">
                                 '.substr($articleOnCurrentPage['text'],0,30).'<a href="pagArticolDiv.php?id='.$articleOnCurrentPage['id'].'">...</a>'.'
                            </div>
                    <div style="clear: both"></div>';
                }
                ?>
        </div>
        <div id="sidebar" style=" width:30%;float: right "><h2>Articole recente</h2>
            <ol>
                <?php
                $articles = query('SELECT * FROM articole');

                foreach ($articles as $article ){
                    echo '<li class="list-group-item"><a href="pagArticolDiv.php?id='.$article['id'].'">'.$article['titlu'].'</a></li>';

                }
                $noOfArticles=count($articles);
                $nrOfPages=ceil($noOfArticles/$maxPerPage);
                ?>
            </ol>
        </div>
        <div style="clear: both"></div>
        <div>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        <?php if ($currentPage > 1): ?>
                        <a class="page-link" href="pagPrincipalDIV.php?page=<?php echo $currentPage - 1; ?>" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <?php endif; ?>
                    <li class="page-item"><a class="page-link" href="pagPrincipalDIV.php?page=<?php echo $currentPage ?>">1</a></li>
                    <li class="page-item"><a class="page-link" href="pagPrincipalDIV.php?page=">2</a></li>
                    <li class="page-item"><a class="page-link" href="pagPrincipalDIV.php?page=">3</a></li>
                    <li class="page-item">
                        <?php if ($currentPage < $nrOfPages): ?>
                        <a class="page-link" href=pagPrincipalDIV.php?page=<?php echo $currentPage + 1; ?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                <?php endif; ?>
                </ul>
            </nav>
        </div>
    </div>
    <div id="footer" class="footer">
        <div> &copy; 2019</div>
     <div style="float: right"> <a href="addArticol.php">+ Add</a></div>
    </div>
</div>
</body>
</html>

<?php

function calculatePrice($price, $name){
    if (strpos($name, 'Iphone')!==false){
        return $price * 0.8;
    }
    return $price * 0.9;
}

function productTile($name, $price, $image="no_image.png", $specs="no specs"){
    echo "<h1>$name</h1>";
    $newPrice = calculatePrice($price, $name);
    echo "<h3><strike>$price RON</strike></h3>";
    echo "<h2>$newPrice RON</h2>";
    echo "<h4>$specs</h4>";
}

$numeIp="Iphone 11 pro";

productTile($numeIp, 4199.99, "iphone.png", "8 GB RAM");
productTile('Iphone 10 pro', 4299.99, "iphone.png", "8 GB RAM");
productTile('Iphone 8 pro', 4399.99, "iphone.png", "8 GB RAM");
productTile('Iphone 9 pro', 4499.99, "iphone.png", "8 GB RAM");

productTile('TV samsung',234.99);

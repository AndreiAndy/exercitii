<?php


function displayTable($table, $player){
    ?>
    <h1>Urmeaza jucatorul: <?php echo $player; ?></h1>
    <table border="1" width="200" height="200" style="text-align: center">
        <?php foreach ($table as $indexI => $row): ?>
            <tr>
                <?php foreach ($row as $indexJ => $item){ ?>
                    <td>
                        <?php
                        if ($item!==false) {
                            echo $item;
                        } else {
                            echo '<a href="xsio.php?indexI='.$indexI.'&indexJ='.$indexJ.'">Liber</a>';
                        }
                        ?>
                    </td>
                <?php } ?>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php
}

function verifyWinner($table){
    $castigator = false;
    $castigatorPeLinie = false;

// validare pe linii
    for ($i=0; $i<3; $i++) {
        if ($table[$i][0] == $table[$i][1] && $table[$i][1] == $table[$i][2] && $table[$i][0] != false) {
            $castigator = $table[$i][0];
            $castigatorPeLinie = " linia ".($i+1);
        }
    }

//validare pe coloane
    for ($i=0; $i<3; $i++) {
        if ($table[0][$i] == $table[1][$i] && $table[1][$i] == $table[2][$i] && $table[0][$i] != false) {
            $castigator = $table[0][$i];
            $castigatorPeLinie = " coloana ".($i+1);
        }
    }

//validare pe diagonala secundara
    if ($table[0][2] == $table[1][1] && $table[1][1] == $table[2][0] && $table[0][2] != false) {
        $castigator = $table[0][2];
        $castigatorPeLinie = ' diagonala secundara';
    }

//validare pe diagonala pricpipala
    if ($table[0][0] == $table[1][1] && $table[1][1] == $table[2][2] && $table[0][0] != false) {
        $castigator = $table[0][0];
        $castigatorPeLinie = ' diagonala principala';
    }

    if ($castigator != false){
        $_SESSION['isWinner'] = true;
        echo "Castigator este $castigator pe $castigatorPeLinie";
    } else {
        $ok = true;

        foreach ($table as $linie) {
            foreach ($linie as $item){
                $ok = $ok && $item!=false;
            }
        }

        if ($ok){
            $_SESSION['isWinner'] = true;
            echo "Meci egal!!";
        }
    }
}

function nextPlayer($currentPlayer='X'){
    if ($currentPlayer == 'X') {
        return 'O';
    } else {
        return 'X';
    }
}

function makeMove($table, $line, $column, $isWinner){


    //verificare actiune valida si nu exista castigator
    if ($table[$line][$column] === false && $isWinner === false) {
        // punere mutare in tabel
        $table[$line][$column] = $_SESSION['player'];
        //stabilire jucator urmator
        $_SESSION['player'] = nextPlayer($_SESSION['player']);
    }

    return $table;
}

function resetGame(){
    $_SESSION['tabela'] = [
        [false, false, false],
        [false, false, false],
        [false, false, false],
    ];
    $_SESSION['player']='X';
    $_SESSION['isWinner']=false;
}
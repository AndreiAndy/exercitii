<?php
include "xsio functions.php";
session_start();
if (!isset($_SESSION['tabela']) || isset($_GET['reseteaza'])) {
    resetGame();
}
//verificare actiune mutare
if (isset($_GET['indexI']) && isset($_GET['indexJ'])) {
    $_SESSION['tabela'] = makeMove($_SESSION['tabela'], $_GET['indexI'], $_GET['indexJ'], $_SESSION['isWinner']);
}

displayTable($_SESSION['tabela'], $_SESSION['player']);
verifyWinner($_SESSION['tabela']);

?>
    <a href="xsio.php?reseteaza=1">Reseteaza joc</a>


<?php
$castigator = false;
$castigatorPeLinie = false;
$tabela = [
    ['x', 'x', 'o'],
    ['o', 'o', 'x'],
    ['x', 'o', 'x'],
];

// validare pe linii
for ($i=0; $i<3; $i++) {
    if ($tabela[$i][0] == $tabela[$i][1] && $tabela[$i][1] == $tabela[$i][2] && $tabela[$i][0] != false) {
        $castigator = $tabela[$i][0];
        $castigatorPeLinie = " linia ".($i+1);
    }
}

//validare pe coloane
for ($i=0; $i<3; $i++) {
    if ($tabela[0][$i] == $tabela[1][$i] && $tabela[1][$i] == $tabela[2][$i] && $tabela[0][$i] != false) {
        $castigator = $tabela[0][$i];
        $castigatorPeLinie = " coloana ".($i+1);
    }
}

//validare pe diagonala secundara
if ($tabela[0][2] == $tabela[1][1] && $tabela[1][1] == $tabela[2][0] && $tabela[0][2] != false) {
    $castigator = $tabela[0][2];
    $castigatorPeLinie = ' diagonala secundara';
}

//validare pe diagonala pricpipala
if ($tabela[0][0] == $tabela[1][1] && $tabela[1][1] == $tabela[2][2] && $tabela[0][0] != false) {
    $castigator = $tabela[0][0];
    $castigatorPeLinie = ' diagonala principala';
}

if ($castigator != false){
    $_SESSION['isWinner']=false;
    echo "Castigator este $castigator pe $castigatorPeLinie";
} else {
    $ok = true;

    foreach ($tabela as $linie) {
        foreach ($linie as $item){
            $ok = $ok && $item!=false;
        }
    }

    if ($ok){
        $_SESSION['isWinner']=false;
        echo "Meci egal!!";
    }
}

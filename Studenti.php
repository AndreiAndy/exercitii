<?php
function sortAsc($x, $y) {
    $column = $_GET['column'];
    return strcmp($x[$column],$y[$column]);
}

function sortDesc($x, $y) {
    $column = $_GET['column'];
    return strcmp($y[$column],$x[$column]);
}
function sortNumAsc($x, $y) {
    $column = $_GET['column'];
    return $x[$column]-$y[$column];
}

function sortNumDesc($x, $y)
{
    $column = $_GET['column'];
    return $y[$column]- $x[$column];
}
$data = array_map('str_getcsv', file('students.csv'));
$header = array_shift($data);
echo"<table border='1'>";
    echo"<tr>";
        foreach ($header as $key => $value){
        echo "<th>$value
<a href='Sudenti.php?direction=asc&column=$key'>&dArr; </a>&nbsp;&nbsp;
<a href='Studenti.php?direction=desc&column=$key'>&uArr; </a>
</th>";
}
            echo"</tr>";
if (isset($_GET['direction'])) {
if ($_GET['direction'] == 'asc') {
    usort($data, 'sortAsc');
} else {
    usort($data, 'sortDesc');
}
}
$maxPerPage = 10;
$currentPage = 1;
if (isset($_GET['page'])) {
    $currentPage = $_GET['page'];
}
$nrOfPages = ceil(count($data)/$maxPerPage);
$startIndex = ($currentPage-1)*$maxPerPage;
$data = array_slice($data, $startIndex, $maxPerPage);
            foreach ($data as $line){
                echo "<tr>";
          foreach ($line as $value){
              echo "<td>$value</td>";
          }
          echo"</tr>";
            }
            echo "</table>";

?>
<p style="text-align:center; font-size: 40px; font-weight: 600;">
    <?php if ($currentPage>1):?>
    <a href="Studenti.php">&laquo;</a>
    <a href="Studenti.php?page=<?php echo $currentPage-1; ?>">&lt;</a>
<?php endif; ?>
<?php echo $currentPage; ?>
<?php if ($currentPage<$nrOfPages):?>
    <a href="Studenti.php?page=<?php echo $currentPage+1; ?>">&gt;</a>
    <a href="Studenti.php?page=<?php echo $nrOfPages; ?>">&raquo;</a>
<?php endif; ?>
</p>
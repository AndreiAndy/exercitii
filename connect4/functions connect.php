<?php


function displayTable($table, $player){
    ?>
    <h1>Urmeaza jucatorul: <?php echo $player; ?></h1>
    <table border="1" width="200" height="200" style="text-align: center">
        <?php foreach ($table as $indexI => $row): ?>
            <tr>
                <?php foreach ($row as $indexJ => $item){ ?>
                    <td>
                        <?php
                        if ($item!==false) {
                            echo $item;
                        } else {
                            if ($indexI==0) {
                                echo '<a href="connect.php?indexJ=' . $indexJ . '">Liber</a>';
                            }
                        }
                        ?>
                    </td>
                <?php } ?>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php
}

function getMaxToLeft($table, $line, $column){
    for ($c=$column; $c>=0; $c--){
        if ($table[$line][$column]!=$table[$line][$c]){
            return $c+1;
        }
    }
    return $column;
}

function verifyWinner($table, $line, $column){
    $castigator = false;
    $castigatorPeLinie = false;

// validare pe linie
    //max to left
    $maxToLeft= getMaxToLeft($table, $line, $column);
    if ($maxToLeft<=3 && $table[$line][$maxToLeft]==$table[$line][$maxToLeft+1] && $table[$line][$maxToLeft+1]==$table[$line][$maxToLeft+2] && $table[$line][$maxToLeft+2]==$table[$line][$maxToLeft+3]){
        $castigator = $table[$line][$column];
        $castigatorPeLinie = " linia ".($line+1);
    }


//validare pe coloana
    if ($line<=2 && $table[$line][$column]==$table[$line+1][$column] && $table[$line+1][$column]==$table[$line+2][$column] && $table[$line+2][$column]==$table[$line+3][$column]){
        $castigator = $table[$line][$column];
        $castigatorPeLinie = " coloana ".($column+1);
    }

//validare pe diagonala secundara


//validare pe diagonala pricpipala
    if ($table[0][0] == $table[1][1] && $table[1][1] == $table[2][2] && $table[0][0] != false) {
        $castigator = $table[0][0];
        $castigatorPeLinie = ' diagonala principala';
    }

    if ($castigator != false){
        $_SESSION['isWinner'] = true;
        echo "Castigator este $castigator pe $castigatorPeLinie";
    } else {
        $ok = true;

        foreach ($table as $linie) {
            foreach ($linie as $item){
                $ok = $ok && $item!=false;
            }
        }

        if ($ok){
            $_SESSION['isWinner'] = true;
            echo "Meci egal!!";
        }
    }
}

function nextPlayer($currentPlayer='X'){
    if ($currentPlayer == 'X') {
        return 'O';
    } else {
        return 'X';
    }
}

function getTopLine($table, $column){
    for($line=0; $line<6; $line++){
        if ($table[$line][$column]!==false){
            return $line-1;
        }
    }

    return 5;
}

function makeMove($table, $column, $isWinner){

    $line = getTopLine($table, $column);
    //verificare actiune valida si nu exista castigator
    if ($table[$line][$column] === false && $isWinner === false) {
        // punere mutare in tabel
        $table[$line][$column] = $_SESSION['player'];
        $_SESSION['currentLine'] = $line;
        //stabilire jucator urmator
        $_SESSION['player'] = nextPlayer($_SESSION['player']);
    }

    return $table;
}

function resetGame(){
    $_SESSION['tabela'] = [
        [false, false, false,false, false, false, false],
        [false, false, false,false, false, false, false],
        [false, false, false,false, false, false, false],
        [false, false, false,false, false, false, false],
        [false, false, false,false, false, false, false],
        [false, false, false,false, false, false, false],
    ];
    $_SESSION['player']='X';
    $_SESSION['isWinner']=false;
}
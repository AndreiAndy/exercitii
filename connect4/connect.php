<?php
include "functions connect.php";
session_start();
if (!isset($_SESSION['tabela']) || isset($_GET['reseteaza'])) {
    resetGame();
}
//verificare actiune mutare
if (isset($_GET['indexJ'])) {
    $_SESSION['tabela'] = makeMove($_SESSION['tabela'], $_GET['indexJ'], $_SESSION['isWinner']);
}

displayTable($_SESSION['tabela'], $_SESSION['player']);
if (isset($_GET['indexJ'])) {
    verifyWinner($_SESSION['tabela'], $_SESSION['currentLine'], $_GET['indexJ']);
}

?>
    <a href="connect.php?reseteaza=1">Reseteaza joc</a>


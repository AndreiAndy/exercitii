<?php
include "functions.php";

$data = query('SELECT * FROM student WHERE id='.intval($_GET['id']));
?>

<form action="processEdit.php?id=<?php echo $_GET['id']; ?>" method="post">
    <table>
        <tr>
            <th>Name</th>
            <td>
                <input type="text" name="name" value="<?php echo $data[0]['nume'];?>" />
            </td>
        </tr>
        <tr>
            <th>Surname</th>
            <td>
                <input type="text" name="surname" value="<?php echo $data[0]['prenume'];?>" />
            </td>
        </tr>
        <tr>
            <th>Class</th>
            <td>
                <select name="class">
                    <option value="9" <?php if ($data[0]['clasa']=='9') {echo "selected='selected'";} ?>>IX</option>
                    <option value="10" <?php if ($data[0]['clasa']=='10') {echo "selected='selected'";} ?>>X</option>
                    <option value="11" <?php if ($data[0]['clasa']=='11') {echo "selected='selected'";} ?>>XI</option>
                    <option value="12" <?php if ($data[0]['clasa']=='12') {echo "selected='selected'";} ?>>XII</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="Save" /> </td>
            <td></td>
        </tr>
    </table>
</form>
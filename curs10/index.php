<?php

include "functions.php";

if (!isset($_SESSION['column'])) {
    $_SESSION['column'] = 'id';
    $_SESSION['direction'] = 'ASC';
    $_SESSION['search'] = '';
}

if (isset($_GET['direction'])) {
    $_SESSION['column'] = $_GET['column'];
    $_SESSION['direction'] = $_GET['direction'];
}

if (isset($_POST['search'])) {
    $_SESSION['search'] = $_POST['search'];
}

$maxPerPage = 10;
$currentPage = 1;
if (isset($_GET['page'])) {
    $currentPage = $_GET['page'];
}

$query = mysqli_query($mysql, 'SELECT count(*) 
      FROM student 
      WHERE nume LIKE "%' . $_SESSION['search'] . '%" OR prenume LIKE "%' . $_SESSION['search'] . '%" 
      ORDER BY '.$_SESSION['column'].' ' . $_SESSION['direction']);


$info = mysqli_fetch_all($query);

$nrOfPages = ceil($info[0][0] / $maxPerPage);

$startIndex = ($currentPage - 1) * $maxPerPage;


$data = query('
      SELECT * 
      FROM student 
      WHERE nume LIKE "%' . $_SESSION['search'] . '%" OR prenume LIKE "%' . $_SESSION['search'] . '%" 
      ORDER BY '.$_SESSION['column'].' ' . $_SESSION['direction']. '
      LIMIT '.$startIndex.','.$maxPerPage.' '
);

$header = array_keys($data[0]);
?>
<form action="index.php" method="post">
    <input type="text" value="<?php echo $_SESSION['search']; ?>" name="search" placeholder="Search"/>
    <input type="submit" value="Cauta">
</form>
<table border='1'>
    <tr>
        <?php
        foreach ($header as $key => $value) {
            echo "<th>$value&nbsp;&nbsp; 
<a href='index.php?direction=ASC&column=$value'>&dArr; </a>&nbsp;&nbsp;
<a href='index.php?direction=DESC&column=$value'>&uArr; </a>

</th>";
        }
        echo "<th>Actions</th></tr>";
        foreach ($data as $line) {
            echo "<tr>";
            foreach ($line as $value) {
                $replacedValue = str_replace($_SESSION['search'], "<font style='color: red;'>" . $_SESSION['search'] . "</font>", $value);
                echo "<td>$replacedValue</td>";
            }
            echo "<td><a href='edit.php?id=".$line['id']."'>Edit</a> <a href='delete.php?id=".$line['id']."'>Delete</a></td></tr>";
        }
?>
</table>

<a href="add.php">+ Add</a>

<p style="text-align:center; font-size: 40px; font-weight: 600;">
    <?php if ($currentPage > 1): ?>
        <a href="index.php">&laquo;</a>
        <a href="index.php?page=<?php echo $currentPage - 1; ?>">&lt;</a>
    <?php endif; ?>
    <?php echo $currentPage; ?>
    <?php if ($currentPage < $nrOfPages): ?>
        <a href="index.php?page=<?php echo $currentPage + 1; ?>">&gt;</a>
        <a href="index.php?page=<?php echo $nrOfPages; ?>">&raquo;</a>
    <?php endif; ?>
</p>
